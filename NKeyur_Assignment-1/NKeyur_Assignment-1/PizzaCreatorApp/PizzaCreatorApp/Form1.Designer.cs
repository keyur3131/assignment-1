﻿namespace PizzaCreatorApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnStartOrder = new System.Windows.Forms.Button();
            this.txtNoOfPizzaz = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblbtnId = new System.Windows.Forms.Label();
            this.btnCheckOut = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSIntroduction = new System.Windows.Forms.RichTextBox();
            this.grpCheese = new System.Windows.Forms.GroupBox();
            this.CHeavy = new System.Windows.Forms.RadioButton();
            this.CNormal = new System.Windows.Forms.RadioButton();
            this.CLight = new System.Windows.Forms.RadioButton();
            this.CNone = new System.Windows.Forms.RadioButton();
            this.grpToppings = new System.Windows.Forms.GroupBox();
            this.chkSpanich = new System.Windows.Forms.CheckBox();
            this.chkTomato = new System.Windows.Forms.CheckBox();
            this.chkHam = new System.Windows.Forms.CheckBox();
            this.chkOlivesGreen = new System.Windows.Forms.CheckBox();
            this.chkOnion = new System.Windows.Forms.CheckBox();
            this.chkPineapple = new System.Windows.Forms.CheckBox();
            this.chkBacon = new System.Windows.Forms.CheckBox();
            this.chkOlivesBlack = new System.Windows.Forms.CheckBox();
            this.chkGreenPeppers = new System.Windows.Forms.CheckBox();
            this.chkMushrooms = new System.Windows.Forms.CheckBox();
            this.chkPepperoni = new System.Windows.Forms.CheckBox();
            this.grpSouce = new System.Windows.Forms.GroupBox();
            this.SHeavy = new System.Windows.Forms.RadioButton();
            this.SNormal = new System.Windows.Forms.RadioButton();
            this.SLight = new System.Windows.Forms.RadioButton();
            this.SNone = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnStartNewOrder = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.grpCheese.SuspendLayout();
            this.grpToppings.SuspendLayout();
            this.grpSouce.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnStartOrder);
            this.panel1.Controls.Add(this.txtNoOfPizzaz);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(13, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(280, 91);
            this.panel1.TabIndex = 0;
            // 
            // btnStartOrder
            // 
            this.btnStartOrder.Location = new System.Drawing.Point(87, 51);
            this.btnStartOrder.Name = "btnStartOrder";
            this.btnStartOrder.Size = new System.Drawing.Size(110, 23);
            this.btnStartOrder.TabIndex = 2;
            this.btnStartOrder.Text = "Start Order";
            this.btnStartOrder.UseVisualStyleBackColor = true;
            this.btnStartOrder.Click += new System.EventHandler(this.btnStartOrder_Click);
            // 
            // txtNoOfPizzaz
            // 
            this.txtNoOfPizzaz.Location = new System.Drawing.Point(115, 21);
            this.txtNoOfPizzaz.Name = "txtNoOfPizzaz";
            this.txtNoOfPizzaz.Size = new System.Drawing.Size(82, 20);
            this.txtNoOfPizzaz.TabIndex = 1;
            this.txtNoOfPizzaz.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoOfPizzaz_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "How many pizzaz?";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblbtnId);
            this.panel2.Controls.Add(this.btnCheckOut);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtSIntroduction);
            this.panel2.Controls.Add(this.grpCheese);
            this.panel2.Controls.Add(this.grpToppings);
            this.panel2.Controls.Add(this.grpSouce);
            this.panel2.Location = new System.Drawing.Point(299, 18);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(457, 574);
            this.panel2.TabIndex = 1;
            // 
            // lblbtnId
            // 
            this.lblbtnId.AutoSize = true;
            this.lblbtnId.Location = new System.Drawing.Point(385, 6);
            this.lblbtnId.Name = "lblbtnId";
            this.lblbtnId.Size = new System.Drawing.Size(0, 13);
            this.lblbtnId.TabIndex = 4;
            this.lblbtnId.Visible = false;
            // 
            // btnCheckOut
            // 
            this.btnCheckOut.BackColor = System.Drawing.Color.Red;
            this.btnCheckOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckOut.Location = new System.Drawing.Point(24, 508);
            this.btnCheckOut.Name = "btnCheckOut";
            this.btnCheckOut.Size = new System.Drawing.Size(409, 34);
            this.btnCheckOut.TabIndex = 3;
            this.btnCheckOut.Text = "Check Out";
            this.btnCheckOut.UseVisualStyleBackColor = false;
            this.btnCheckOut.Click += new System.EventHandler(this.btnCheckOut_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 369);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Special Introduction";
            // 
            // txtSIntroduction
            // 
            this.txtSIntroduction.Location = new System.Drawing.Point(26, 390);
            this.txtSIntroduction.Name = "txtSIntroduction";
            this.txtSIntroduction.Size = new System.Drawing.Size(407, 96);
            this.txtSIntroduction.TabIndex = 5;
            this.txtSIntroduction.Text = "";
            // 
            // grpCheese
            // 
            this.grpCheese.Controls.Add(this.CHeavy);
            this.grpCheese.Controls.Add(this.CNormal);
            this.grpCheese.Controls.Add(this.CLight);
            this.grpCheese.Controls.Add(this.CNone);
            this.grpCheese.Location = new System.Drawing.Point(26, 279);
            this.grpCheese.Name = "grpCheese";
            this.grpCheese.Size = new System.Drawing.Size(407, 71);
            this.grpCheese.TabIndex = 4;
            this.grpCheese.TabStop = false;
            this.grpCheese.Text = "Cheese";
            // 
            // CHeavy
            // 
            this.CHeavy.AutoSize = true;
            this.CHeavy.Location = new System.Drawing.Point(316, 33);
            this.CHeavy.Name = "CHeavy";
            this.CHeavy.Size = new System.Drawing.Size(56, 17);
            this.CHeavy.TabIndex = 3;
            this.CHeavy.TabStop = true;
            this.CHeavy.Text = "Heavy";
            this.CHeavy.UseVisualStyleBackColor = true;
            this.CHeavy.Click += new System.EventHandler(this.CHeavy_Click);
            // 
            // CNormal
            // 
            this.CNormal.AutoSize = true;
            this.CNormal.Location = new System.Drawing.Point(216, 33);
            this.CNormal.Name = "CNormal";
            this.CNormal.Size = new System.Drawing.Size(58, 17);
            this.CNormal.TabIndex = 2;
            this.CNormal.TabStop = true;
            this.CNormal.Text = "Normal";
            this.CNormal.UseVisualStyleBackColor = true;
            this.CNormal.Click += new System.EventHandler(this.CNormal_Click);
            // 
            // CLight
            // 
            this.CLight.AutoSize = true;
            this.CLight.Location = new System.Drawing.Point(115, 33);
            this.CLight.Name = "CLight";
            this.CLight.Size = new System.Drawing.Size(48, 17);
            this.CLight.TabIndex = 1;
            this.CLight.TabStop = true;
            this.CLight.Text = "Light";
            this.CLight.UseVisualStyleBackColor = true;
            this.CLight.Click += new System.EventHandler(this.CLight_Click);
            // 
            // CNone
            // 
            this.CNone.AutoSize = true;
            this.CNone.Location = new System.Drawing.Point(22, 33);
            this.CNone.Name = "CNone";
            this.CNone.Size = new System.Drawing.Size(51, 17);
            this.CNone.TabIndex = 0;
            this.CNone.TabStop = true;
            this.CNone.Text = "None";
            this.CNone.UseVisualStyleBackColor = true;
            this.CNone.Click += new System.EventHandler(this.CNone_Click);
            // 
            // grpToppings
            // 
            this.grpToppings.Controls.Add(this.chkSpanich);
            this.grpToppings.Controls.Add(this.chkTomato);
            this.grpToppings.Controls.Add(this.chkHam);
            this.grpToppings.Controls.Add(this.chkOlivesGreen);
            this.grpToppings.Controls.Add(this.chkOnion);
            this.grpToppings.Controls.Add(this.chkPineapple);
            this.grpToppings.Controls.Add(this.chkBacon);
            this.grpToppings.Controls.Add(this.chkOlivesBlack);
            this.grpToppings.Controls.Add(this.chkGreenPeppers);
            this.grpToppings.Controls.Add(this.chkMushrooms);
            this.grpToppings.Controls.Add(this.chkPepperoni);
            this.grpToppings.Location = new System.Drawing.Point(26, 104);
            this.grpToppings.Name = "grpToppings";
            this.grpToppings.Size = new System.Drawing.Size(407, 156);
            this.grpToppings.TabIndex = 1;
            this.grpToppings.TabStop = false;
            this.grpToppings.Text = "Toppings";
            // 
            // chkSpanich
            // 
            this.chkSpanich.AutoSize = true;
            this.chkSpanich.Location = new System.Drawing.Point(316, 92);
            this.chkSpanich.Name = "chkSpanich";
            this.chkSpanich.Size = new System.Drawing.Size(65, 17);
            this.chkSpanich.TabIndex = 10;
            this.chkSpanich.Text = "Spanich";
            this.chkSpanich.UseVisualStyleBackColor = true;
            this.chkSpanich.CheckedChanged += new System.EventHandler(this.chkSpanich_CheckedChanged);
            // 
            // chkTomato
            // 
            this.chkTomato.AutoSize = true;
            this.chkTomato.Location = new System.Drawing.Point(316, 64);
            this.chkTomato.Name = "chkTomato";
            this.chkTomato.Size = new System.Drawing.Size(62, 17);
            this.chkTomato.TabIndex = 9;
            this.chkTomato.Text = "Tomato";
            this.chkTomato.UseVisualStyleBackColor = true;
            this.chkTomato.CheckedChanged += new System.EventHandler(this.chkTomato_CheckedChanged);
            // 
            // chkHam
            // 
            this.chkHam.AutoSize = true;
            this.chkHam.Location = new System.Drawing.Point(316, 36);
            this.chkHam.Name = "chkHam";
            this.chkHam.Size = new System.Drawing.Size(48, 17);
            this.chkHam.TabIndex = 8;
            this.chkHam.Text = "Ham";
            this.chkHam.UseVisualStyleBackColor = true;
            this.chkHam.CheckedChanged += new System.EventHandler(this.chkHam_CheckedChanged);
            // 
            // chkOlivesGreen
            // 
            this.chkOlivesGreen.AutoSize = true;
            this.chkOlivesGreen.Location = new System.Drawing.Point(172, 120);
            this.chkOlivesGreen.Name = "chkOlivesGreen";
            this.chkOlivesGreen.Size = new System.Drawing.Size(90, 17);
            this.chkOlivesGreen.TabIndex = 7;
            this.chkOlivesGreen.Text = "Olives, Green";
            this.chkOlivesGreen.UseVisualStyleBackColor = true;
            this.chkOlivesGreen.CheckedChanged += new System.EventHandler(this.chkOlivesGreen_CheckedChanged);
            // 
            // chkOnion
            // 
            this.chkOnion.AutoSize = true;
            this.chkOnion.Location = new System.Drawing.Point(172, 92);
            this.chkOnion.Name = "chkOnion";
            this.chkOnion.Size = new System.Drawing.Size(54, 17);
            this.chkOnion.TabIndex = 6;
            this.chkOnion.Text = "Onion";
            this.chkOnion.UseVisualStyleBackColor = true;
            this.chkOnion.CheckedChanged += new System.EventHandler(this.chkOnion_CheckedChanged);
            // 
            // chkPineapple
            // 
            this.chkPineapple.AutoSize = true;
            this.chkPineapple.Location = new System.Drawing.Point(172, 64);
            this.chkPineapple.Name = "chkPineapple";
            this.chkPineapple.Size = new System.Drawing.Size(73, 17);
            this.chkPineapple.TabIndex = 5;
            this.chkPineapple.Text = "Pineapple";
            this.chkPineapple.UseVisualStyleBackColor = true;
            this.chkPineapple.CheckedChanged += new System.EventHandler(this.chkPineapple_CheckedChanged);
            // 
            // chkBacon
            // 
            this.chkBacon.AutoSize = true;
            this.chkBacon.Location = new System.Drawing.Point(172, 36);
            this.chkBacon.Name = "chkBacon";
            this.chkBacon.Size = new System.Drawing.Size(57, 17);
            this.chkBacon.TabIndex = 4;
            this.chkBacon.Text = "Bacon";
            this.chkBacon.UseVisualStyleBackColor = true;
            this.chkBacon.CheckedChanged += new System.EventHandler(this.chkBacon_CheckedChanged);
            // 
            // chkOlivesBlack
            // 
            this.chkOlivesBlack.AutoSize = true;
            this.chkOlivesBlack.Location = new System.Drawing.Point(22, 120);
            this.chkOlivesBlack.Name = "chkOlivesBlack";
            this.chkOlivesBlack.Size = new System.Drawing.Size(88, 17);
            this.chkOlivesBlack.TabIndex = 3;
            this.chkOlivesBlack.Text = "Olives, Black";
            this.chkOlivesBlack.UseVisualStyleBackColor = true;
            this.chkOlivesBlack.CheckedChanged += new System.EventHandler(this.chkOlivesBlack_CheckedChanged);
            // 
            // chkGreenPeppers
            // 
            this.chkGreenPeppers.AutoSize = true;
            this.chkGreenPeppers.Location = new System.Drawing.Point(22, 92);
            this.chkGreenPeppers.Name = "chkGreenPeppers";
            this.chkGreenPeppers.Size = new System.Drawing.Size(97, 17);
            this.chkGreenPeppers.TabIndex = 2;
            this.chkGreenPeppers.Text = "Green Peppers";
            this.chkGreenPeppers.UseVisualStyleBackColor = true;
            this.chkGreenPeppers.CheckedChanged += new System.EventHandler(this.chkGreenPeppers_CheckedChanged);
            // 
            // chkMushrooms
            // 
            this.chkMushrooms.AutoSize = true;
            this.chkMushrooms.Location = new System.Drawing.Point(22, 64);
            this.chkMushrooms.Name = "chkMushrooms";
            this.chkMushrooms.Size = new System.Drawing.Size(80, 17);
            this.chkMushrooms.TabIndex = 1;
            this.chkMushrooms.Text = "Mushrooms";
            this.chkMushrooms.UseVisualStyleBackColor = true;
            this.chkMushrooms.CheckedChanged += new System.EventHandler(this.chkMushrooms_CheckedChanged);
            // 
            // chkPepperoni
            // 
            this.chkPepperoni.AutoSize = true;
            this.chkPepperoni.Location = new System.Drawing.Point(22, 36);
            this.chkPepperoni.Name = "chkPepperoni";
            this.chkPepperoni.Size = new System.Drawing.Size(74, 17);
            this.chkPepperoni.TabIndex = 0;
            this.chkPepperoni.Text = "Pepperoni";
            this.chkPepperoni.UseVisualStyleBackColor = true;
            this.chkPepperoni.CheckedChanged += new System.EventHandler(this.chkPepperoni_CheckedChanged);
            // 
            // grpSouce
            // 
            this.grpSouce.Controls.Add(this.SHeavy);
            this.grpSouce.Controls.Add(this.SNormal);
            this.grpSouce.Controls.Add(this.SLight);
            this.grpSouce.Controls.Add(this.SNone);
            this.grpSouce.Location = new System.Drawing.Point(26, 18);
            this.grpSouce.Name = "grpSouce";
            this.grpSouce.Size = new System.Drawing.Size(407, 72);
            this.grpSouce.TabIndex = 0;
            this.grpSouce.TabStop = false;
            this.grpSouce.Text = "Sauce";
            // 
            // SHeavy
            // 
            this.SHeavy.AutoSize = true;
            this.SHeavy.Location = new System.Drawing.Point(319, 33);
            this.SHeavy.Name = "SHeavy";
            this.SHeavy.Size = new System.Drawing.Size(56, 17);
            this.SHeavy.TabIndex = 3;
            this.SHeavy.TabStop = true;
            this.SHeavy.Text = "Heavy";
            this.SHeavy.UseVisualStyleBackColor = true;
            this.SHeavy.Click += new System.EventHandler(this.SHeavy_Click);
            // 
            // SNormal
            // 
            this.SNormal.AutoSize = true;
            this.SNormal.Location = new System.Drawing.Point(213, 33);
            this.SNormal.Name = "SNormal";
            this.SNormal.Size = new System.Drawing.Size(58, 17);
            this.SNormal.TabIndex = 2;
            this.SNormal.TabStop = true;
            this.SNormal.Text = "Normal";
            this.SNormal.UseVisualStyleBackColor = true;
            this.SNormal.Click += new System.EventHandler(this.SNormal_Click);
            // 
            // SLight
            // 
            this.SLight.AutoSize = true;
            this.SLight.Location = new System.Drawing.Point(114, 33);
            this.SLight.Name = "SLight";
            this.SLight.Size = new System.Drawing.Size(48, 17);
            this.SLight.TabIndex = 1;
            this.SLight.TabStop = true;
            this.SLight.Text = "Light";
            this.SLight.UseVisualStyleBackColor = true;
            this.SLight.Click += new System.EventHandler(this.SLight_Click);
            // 
            // SNone
            // 
            this.SNone.AutoSize = true;
            this.SNone.Location = new System.Drawing.Point(22, 33);
            this.SNone.Name = "SNone";
            this.SNone.Size = new System.Drawing.Size(51, 17);
            this.SNone.TabIndex = 0;
            this.SNone.TabStop = true;
            this.SNone.Text = "None";
            this.SNone.UseVisualStyleBackColor = true;
            this.SNone.Click += new System.EventHandler(this.SNone_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 115);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(280, 477);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // btnStartNewOrder
            // 
            this.btnStartNewOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnStartNewOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartNewOrder.Location = new System.Drawing.Point(766, 527);
            this.btnStartNewOrder.Name = "btnStartNewOrder";
            this.btnStartNewOrder.Size = new System.Drawing.Size(409, 34);
            this.btnStartNewOrder.TabIndex = 6;
            this.btnStartNewOrder.Text = "Start New Order";
            this.btnStartNewOrder.UseVisualStyleBackColor = false;
            this.btnStartNewOrder.Click += new System.EventHandler(this.btnStartNewOrder_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(773, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 26);
            this.label3.TabIndex = 3;
            this.label3.Text = "Order Summary";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(766, 123);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(212, 398);
            this.flowLayoutPanel1.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 600);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnStartNewOrder);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pizza Order";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.grpCheese.ResumeLayout(false);
            this.grpCheese.PerformLayout();
            this.grpToppings.ResumeLayout(false);
            this.grpToppings.PerformLayout();
            this.grpSouce.ResumeLayout(false);
            this.grpSouce.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnStartOrder;
        private System.Windows.Forms.TextBox txtNoOfPizzaz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox grpSouce;
        private System.Windows.Forms.RadioButton SHeavy;
        private System.Windows.Forms.RadioButton SNormal;
        private System.Windows.Forms.RadioButton SLight;
        private System.Windows.Forms.RadioButton SNone;
        private System.Windows.Forms.GroupBox grpToppings;
        private System.Windows.Forms.CheckBox chkSpanich;
        private System.Windows.Forms.CheckBox chkTomato;
        private System.Windows.Forms.CheckBox chkHam;
        private System.Windows.Forms.CheckBox chkOlivesGreen;
        private System.Windows.Forms.CheckBox chkOnion;
        private System.Windows.Forms.CheckBox chkPineapple;
        private System.Windows.Forms.CheckBox chkBacon;
        private System.Windows.Forms.CheckBox chkOlivesBlack;
        private System.Windows.Forms.CheckBox chkGreenPeppers;
        private System.Windows.Forms.CheckBox chkMushrooms;
        private System.Windows.Forms.CheckBox chkPepperoni;
        private System.Windows.Forms.GroupBox grpCheese;
        private System.Windows.Forms.RadioButton CHeavy;
        private System.Windows.Forms.RadioButton CNormal;
        private System.Windows.Forms.RadioButton CLight;
        private System.Windows.Forms.RadioButton CNone;
        private System.Windows.Forms.Button btnCheckOut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox txtSIntroduction;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnStartNewOrder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblbtnId;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}

