﻿using PizzaCreatorApp.Common;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace PizzaCreatorApp
{
    public partial class Form1 : Form
    {
        public static string souceType = string.Empty;
        public static string cheeseType = string.Empty;
        StringBuilder sb = new StringBuilder();
        
        public Form1()
        {
            InitializeComponent();
            panel2.Enabled = false;
        }
        private void btnStartOrder_Click(object sender, EventArgs e)
        {
            try
            {
                // Getting intput quantity.
                int qty = Convert.ToInt32(txtNoOfPizzaz.Text);
                if (qty < 1 || qty > 9)
                {
                    MessageBox.Show("Please enter quantity from 1 to 9.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNoOfPizzaz.Focus();
                }
                else
                {
                    panel2.Enabled = true;
                    int z = 0;
                    for (int i = 0; i < qty; i++)
                    {
                        Button btn = new Button();
                        btn.Width = 258;
                        btn.Height = 217;
                        //btn.Text = i.ToString();
                        btn.Name = "btn" + i.ToString();
                        btn.Margin = new Padding(18, 18, 18, 18);
                        btn.Click += new EventHandler(this.button_click);
                        tableLayoutPanel1.Controls.Add(btn, 0, z);
                        z += 1;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtNoOfPizzaz_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verify that the pressed key isn't CTRL or any non-numeric digit
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            try
            {
                Pizza.UpdateInstruction(lblbtnId.Text,txtSIntroduction.Text,sb.ToString());
                this.Width = 1204;
                DataTable dt = Pizza.DisplayDataForSummary();
                int rowCount = 1;
                foreach (DataRow dr in dt.Rows)
                {
                   
                    Label lblPizzaNo = new Label();
                    lblPizzaNo.Text = "Pizza# " + rowCount.ToString();
                    flowLayoutPanel1.Controls.Add(lblPizzaNo);

                    Label lblToppings = new Label();
                    lblToppings.Text = "Toppings:";
                    flowLayoutPanel1.Controls.Add(lblToppings);


                    string s = dr["Toppings"].ToString();
                    string[] arr = s.Split(',');
                    foreach (string item in arr)
                    {
                        Label lblToppingsInfo = new Label();
                        lblToppingsInfo.Text = "  " + item;//+ Environment.NewLine;
                        flowLayoutPanel1.Controls.Add(lblToppingsInfo);
                    }
                    Label lblIntro = new Label();
                    lblIntro.Text = "Introduction : "+ dr["SpecialIntro"].ToString();                    
                    flowLayoutPanel1.Controls.Add(lblIntro);
                    rowCount += 1 ;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        void button_click(object sender, EventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                clearFields();

                lblbtnId.Text = btn.Name;
                bool ifExists = Pizza.GetPizzaInfoByButtonId(lblbtnId.Text);
                if (ifExists)
                {
                    SqlCeDataReader sdr = Pizza.DisplayDataIfExists(lblbtnId.Text);
                    while (sdr.Read())
                    {
                        string sauceResult = sdr["SauceType"].ToString();
                        string cheeseResult = sdr["CheeseType"].ToString();

                        // Checking the value for sauce result.
                        if (sauceResult == "None")
                        {
                            SNone.Checked = true;
                        }
                        else if (sauceResult == "Light")
                        {
                            SLight.Checked = true;
                        }
                        else if (sauceResult == "Normal")
                        {
                            SNormal.Checked = true;
                        }
                        else if (sauceResult == "Heavy")
                        {
                            SHeavy.Checked = true;
                        }
                        // Checking the value for cheese result.
                        if (cheeseResult == "None")
                        {
                            CNone.Checked = true;
                        }
                        else if (cheeseResult == "Light")
                        {
                            CLight.Checked = true;
                        }
                        else if (cheeseResult == "Normal")
                        {
                            CNormal.Checked = true;
                        }
                        else if (cheeseResult == "Heavy")
                        {
                            CHeavy.Checked = true;
                        }
                        txtSIntroduction.Text = sdr["SpecialIntro"].ToString();
                        #region 
                        string s = sdr["Toppings"].ToString();
                        string[] str = s.Split(',');
                        foreach (string x in str)
                        {
                            if (x == "Pepperoni")
                            {
                                chkPepperoni.Checked = true;
                            }
                            else if (x == "Bacon")
                            {
                                chkBacon.Checked = true;
                            }
                            else if (x == "Ham")
                            {
                                chkHam.Checked = true;
                            }
                            else if (x == "Mushrooms")
                            {
                                chkMushrooms.Checked = true;
                            }
                            else if (x == "Pineapple")
                            {
                                chkPineapple.Checked = true;
                            }
                            else if (x == "Tomato")
                            {
                                chkTomato.Checked = true;
                            }
                            else if (x == "Green Peppers")
                            {
                                chkGreenPeppers.Checked = true;
                            }
                            else if (x == "Onion")
                            {
                                chkOnion.Checked = true;
                            }
                            else if (x == "Spanich")
                            {
                                chkSpanich.Checked = true;
                            }
                            else if (x == "Olives Black")
                            {
                                chkOlivesBlack.Checked = true;
                            }
                            else if (x == "Olives Green")
                            {
                                chkOlivesGreen.Checked = true;
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    Pizza.AddButtonId(lblbtnId.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        /// <summary>
        /// Reset fields/Clear db.
        /// </summary>
        public void clearFields()
        {
            foreach (Control c in grpToppings.Controls)
            {
                if (c.GetType() == typeof(CheckBox))
                {
                    if (((CheckBox)c).Checked)
                    {
                        ((CheckBox)c).Checked = false;
                    }
                }
            }
            foreach (Control c in grpSouce.Controls)
            {
                if (c.GetType() == typeof(RadioButton))
                {
                    if (((RadioButton)c).Checked)
                    {
                        ((RadioButton)c).Checked = false;
                    }
                }
            }
            foreach (Control c in grpCheese.Controls)
            {
                if (c.GetType() == typeof(RadioButton))
                {
                    if (((RadioButton)c).Checked)
                    {
                        ((RadioButton)c).Checked = false;
                    }
                }
            }
            txtSIntroduction.Text = string.Empty;
            txtNoOfPizzaz.Text = string.Empty;
            txtNoOfPizzaz.Focus();
            sb.Clear();
        }

        /// <summary>
        /// Reset fields/Clear db.
        /// </summary>
        public void clearDbAndFields()
        {
            foreach (Control c in grpToppings.Controls)
            {
                if (c.GetType() == typeof(CheckBox))
                {
                    if (((CheckBox)c).Checked)
                    {
                        ((CheckBox)c).Checked = false;
                    }
                }
            }
            foreach (Control c in grpSouce.Controls)
            {
                if (c.GetType() == typeof(RadioButton))
                {
                    if (((RadioButton)c).Checked)
                    {
                        ((RadioButton)c).Checked = false;
                    }
                }
            }
            foreach (Control c in grpCheese.Controls)
            {
                if (c.GetType() == typeof(RadioButton))
                {
                    if (((RadioButton)c).Checked)
                    {
                        ((RadioButton)c).Checked = false;
                    }
                }
            }
            foreach (Control c in tableLayoutPanel1.Controls)
            {
                tableLayoutPanel1.Controls.Clear();
                tableLayoutPanel1.RowStyles.Clear();
            }

            txtSIntroduction.Text = string.Empty;
            txtNoOfPizzaz.Text = string.Empty;
            txtNoOfPizzaz.Focus();
            sb.Clear();
            panel2.Enabled = false;
            Pizza.DeleteData();
        }

        private void btnStartNewOrder_Click(object sender, EventArgs e)
        {
            clearDbAndFields();
            this.Width = 783;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            clearDbAndFields();
        }

        #region CheckBoxes       
        private void chkPepperoni_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Pepperoni").Append(",");
        }

        private void chkBacon_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Bacon").Append(",");
        }

        private void chkHam_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Ham").Append(",");
        }

        private void chkMushrooms_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Mushrooms").Append(",");
        }

        private void chkPineapple_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Pineapple").Append(",");
        }

        private void chkTomato_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Tomato").Append(",");
        }

        private void chkGreenPeppers_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Green Peppers").Append(",");
        }

        private void chkOnion_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Onion").Append(",");
        }

        private void chkSpanich_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Spanich").Append(",");
        }

        private void chkOlivesBlack_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Olives Black").Append(",");
        }

        private void chkOlivesGreen_CheckedChanged(object sender, EventArgs e)
        {
            sb.Append("Olives Green").Append(",");
        }
        #endregion

        private void SNone_Click(object sender, EventArgs e)
        {
            souceType = "None";
            Pizza.AddSauce(lblbtnId.Text, souceType, cheeseType, txtSIntroduction.Text, sb.ToString());
        }

        private void SLight_Click(object sender, EventArgs e)
        {
            souceType = "Light";
            Pizza.AddSauce(lblbtnId.Text, souceType, cheeseType, txtSIntroduction.Text, sb.ToString());
        }

        private void SNormal_Click(object sender, EventArgs e)
        {
            souceType = "Normal";
            Pizza.AddSauce(lblbtnId.Text, souceType, cheeseType, txtSIntroduction.Text, sb.ToString());
        }

        private void SHeavy_Click(object sender, EventArgs e)
        {
            souceType = "Heavy";
            Pizza.AddSauce(lblbtnId.Text, souceType, cheeseType, txtSIntroduction.Text, sb.ToString());
        }

        private void CNone_Click(object sender, EventArgs e)
        {
            cheeseType = "None";
            Pizza.AddSauce(lblbtnId.Text, souceType, cheeseType, txtSIntroduction.Text, sb.ToString());
        }

        private void CLight_Click(object sender, EventArgs e)
        {
            cheeseType = "Light";
            Pizza.AddSauce(lblbtnId.Text, souceType, cheeseType, txtSIntroduction.Text, sb.ToString());
        }

        private void CNormal_Click(object sender, EventArgs e)
        {
            cheeseType = "Normal";
            Pizza.AddSauce(lblbtnId.Text, souceType, cheeseType, txtSIntroduction.Text, sb.ToString());
        }

        private void CHeavy_Click(object sender, EventArgs e)
        {
            cheeseType = "Heavy";
            Pizza.AddSauce(lblbtnId.Text, souceType, cheeseType, txtSIntroduction.Text, sb.ToString());
        }
    }
}
