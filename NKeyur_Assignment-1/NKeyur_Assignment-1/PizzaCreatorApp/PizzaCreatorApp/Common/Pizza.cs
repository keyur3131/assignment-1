﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaCreatorApp.Common
{
    public static class Pizza
    {
        /// <summary>
        /// Save button id when user click on any button. 
        /// </summary>
        /// <param name="btnId"></param>
        public static void AddButtonId(string btnId)
        {
            using (SqlCeConnection con = new SqlCeConnection(DbCon.GetLocalDbConnection()))
            {
                SqlCeCommand cmd = new SqlCeCommand("Insert Into tblSauce (btnId) Values (@btnId)", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@btnId", btnId);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
        
        /// <summary>
        /// Will update data against button id
        /// </summary>
        /// <param name="btnId"></param>
        /// <param name="sauceType"></param>
        /// <param name="cheeseType"></param>
        /// <param name="SpecialIntro"></param>
        public static void AddSauce(string btnId, string sauceType, string cheeseType, string SpecialIntro,string toppings)
        {
            using (SqlCeConnection con = new SqlCeConnection(DbCon.GetLocalDbConnection()))
            {
                SqlCeCommand cmd = new SqlCeCommand("Update tblSauce Set btnId=@btnId,SauceType=@SauceType,CheeseType=@CheeseType,SpecialIntro=@SpecialIntro,Toppings=@Toppings Where btnId='" + btnId + "'", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@btnId", btnId);
                cmd.Parameters.AddWithValue("@SauceType", sauceType);
                cmd.Parameters.AddWithValue("@CheeseType", cheeseType);
                cmd.Parameters.AddWithValue("@SpecialIntro", SpecialIntro);
                cmd.Parameters.AddWithValue("@Toppings", toppings);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
        
        /// <summary>
        /// Will get data against button id when user click again on any button.
        /// </summary>
        /// <param name="btnId"></param>
        /// <returns></returns>
        public static bool GetPizzaInfoByButtonId(string btnId)
        {
            bool ifExists = false;
            using (SqlCeConnection con = new SqlCeConnection(DbCon.GetLocalDbConnection()))
            {
                SqlCeCommand cmd = new SqlCeCommand("Select * From tblSauce Where btnId='" + btnId + "'", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@btnId", btnId);
                con.Open();
                int ret_val = Convert.ToInt32(cmd.ExecuteScalar());
                if (ret_val != 0)
                {
                    ifExists = true;
                }
            }
            return ifExists;
        }    
        
        /// <summary>
        /// Will return the data reader will get fill controls from that data.
        /// </summary>
        /// <param name="btnId"></param>
        /// <returns></returns>
        public static SqlCeDataReader DisplayDataIfExists(string btnId)
        {
            SqlCeDataReader sdr = null;
            SqlCeConnection con = new SqlCeConnection(DbCon.GetLocalDbConnection());
            SqlCeCommand cmd = new SqlCeCommand("select * From tblSauce Where btnId=@btnId", con);
            con.Open();
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@btnId", btnId);
            sdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return sdr;
        }     
        
        /// <summary>
        /// When user press the Start New Order button we will clear all the fields.
        /// </summary>
        /// <returns></returns>
        public static DataTable DeleteData()
        {
            DataTable dt = new DataTable();
            using (SqlCeConnection con = new SqlCeConnection(DbCon.GetLocalDbConnection()))
            {
                SqlCeCommand cmd = new SqlCeCommand("Delete From tblSauce", con);
                con.Open();
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }
            return dt;
        }
        
        /// <summary>
        /// Will get all data for summary. 
        /// </summary>
        /// <returns></returns>
        public static DataTable DisplayDataForSummary()
        {
            DataTable dt = new DataTable();
            using (SqlCeConnection con = new SqlCeConnection(DbCon.GetLocalDbConnection()))
            {
                SqlCeCommand cmd = new SqlCeCommand("select * From tblSauce", con);
                con.Open();
                cmd.CommandType = CommandType.Text;
                SqlCeDataAdapter sda = new SqlCeDataAdapter(cmd);
                sda.Fill(dt);
            }
            return dt;
        }
        /// <summary>
        /// Update Instruction if user does'nt choose the option from cheese section.
        /// </summary>
        /// <param name="btnId"></param>
        public static void UpdateInstruction(string btnId,string SpecialIntro,string Toppings)
        {
            using (SqlCeConnection con = new SqlCeConnection(DbCon.GetLocalDbConnection()))
            {
                SqlCeCommand cmd = new SqlCeCommand("Update tblSauce Set SpecialIntro=@SpecialIntro,Toppings=@Toppings Where btnId='" + btnId+"'", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@btnId", btnId);
                cmd.Parameters.AddWithValue("@SpecialIntro", SpecialIntro);
                cmd.Parameters.AddWithValue("@Toppings", Toppings);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
