﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaCreatorApp.Common
{
    public static class DbCon
    {
        /// <summary>
        /// This method allows us to connect with local db
        /// </summary>
        /// <returns></returns>
        public static string GetLocalDbConnection()
        {
            string conString = string.Empty;
            conString = "Data Source=|DataDirectory|\\db\\MyDb.sdf;Password=asd!@#;";
            return conString;
        }
    }
}
